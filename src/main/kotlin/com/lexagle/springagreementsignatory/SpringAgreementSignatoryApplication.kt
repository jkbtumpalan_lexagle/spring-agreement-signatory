package com.lexagle.springagreementsignatory

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringAgreementSignatoryApplication

fun main(args: Array<String>) {
    runApplication<SpringAgreementSignatoryApplication>(*args)
}
